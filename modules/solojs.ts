import * as path from 'path';

import { log } from './solojs/core';
import { Server } from './solojs/http';
import { RouteHandler, ControllerHandler } from './solojs/control';
import { ViewEngine } from './solojs/view';


let base = '/home/chris/Documents/Projects/node-solo/test-app';
let appDir = path.join(base, 'app');
let controllersDir = path.join(appDir, 'controllers');
let viewsDir = path.join(appDir, 'views');
let configDir = path.join(base, 'config');

let routeHandler = new RouteHandler();
let routes = require(path.join(configDir, 'routes.js'));
routes.apply(this, [routeHandler]);

console.log(routeHandler.routes);

let controllerHandler = new ControllerHandler({ controllerRoot: controllersDir });
let viewEngine = new ViewEngine({ templatePaths: [ viewsDir ] });

let server = Server.make();

server.start((req, res) => {
    console.log(req.method, req.url);
    let route = routeHandler.parse(req.url, req.method);
    if (route) {
        controllerHandler.call(route.action.controller, route.action.action, req, res);
    }
}).listen(8081);