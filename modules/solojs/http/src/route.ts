import * as _ from 'lodash';
import * as pathRegexp from 'path-to-regexp';

export interface RouteOptions {
    to: string;
    redirect?: string;
    status?: number;
    constraints?: {[param: string]: RegExp};
    via: string | string[];
}

export interface RouteAction {
    controller: string;
    action: string;
}

type AllRouteOptions = RouteOptions & pathRegexp.RegExpOptions & pathRegexp.ParseOptions;

export class Route {

    protected path: string;

    protected regexp: RegExp;

    protected params: Object;

    protected keys: pathRegexp.Key[];

    protected _action: RouteAction;

    protected _method: string | string[];

    protected options: RouteOptions

    constructor(path: string, options: AllRouteOptions) {

        this.options = options;
        this._action = Route.parseAction(options.to);
        this._method = options.via;
        this.path = path;
        this.keys = [];
        this.regexp = pathRegexp(path, this.keys, options);

        if (path === '/' && options.end === false) {
            this.regexp['fast_slash'] = true;
        }
    }

    public get method(): string[] {
        return _.isArray(this._method) ? this._method : [ this._method ];
    }

    public get action(): RouteAction {
        return this._action;
    }

    public match(path: string): boolean {
        if (!path) {
            this.params = undefined;
            this.path = undefined;
            return false;
        }

        if (this.regexp['fast_slash']) {
            this.params = {};
            this.path = '';
            return true;
        }

        let matcher = this.regexp.exec(path);

        if (!matcher) {
            this.params = undefined;
            this.path = undefined;
            return false;
        }

        this.params = {};
        this.path = matcher[0];

        let keys = this.keys;
        let params = this.params;

        for (let i = 1; i < matcher.length; i++) {
            let key = keys[i - 1];
            let prop = key.name;
            let val = this.decodeParameter(matcher[i]);

            if (val && !(Object.prototype.hasOwnProperty.call(params, prop))) {
                params[prop] = val;
            }
        }

        return true;
    }

    public decodeParameter(param: string): string {
        if (!_.isString(param) || param.length === 0) {
            return param;
        }

        try {
            return decodeURIComponent(param);
        } catch(e) {
            if (e instanceof URIError) {
                e.message = `Failed to decode param '${param}'`;
                e['status'] = e['statusCode'] = 400;
            }

            throw e;
        }
    }

    public static parseAction(action: string): RouteAction {
        let parts = action.split('#');

        if (parts.length < 2)
            throw new Error('parseAction failed. Controllers and actions need to be separated by a #');

        return {
            controller: parts[0],
            action: parts[1]
        }
    }

}