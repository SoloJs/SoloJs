import { EventEmitter } from 'events';

import * as _ from 'lodash';

import { init as initMiddleware } from './middleware/init';
import { Logger } from '../../core';
import { Request } from './request';
import { Response } from './response';
import { Router } from './router';
import { Route, RouteOptions } from './route';

export interface ApplicationOptions {
    logLevel?: string;
}

const trustProxyDefaultSymbol = '@@symbol:trust_proxy_default';

export class Application extends EventEmitter {

    public request: Request;

    public response: Response;

    public cache: Object;

    public engines: Object;

    public settings: Object;

    public log: Logger;

    public locals: Object;

    public mountpath: string;

    private _router: Router;

    constructor(options: ApplicationOptions = {}) {
        super();

        this.log = new Logger(options.logLevel || 'info');
        this.applyDefaults();
    }

    public get path(): string {}

    public enabled(setting: string): boolean {
        return Boolean(this.set(setting));
    }

    public disabled(setting: string): boolean {
        return Boolean(this.set(setting));
    }

    public enable(param: string) {
        this.set(param, true);
    }

    public disable(param: string) {
        this.set(param, false);
    }

    public lazyRouter() {
        if (!this._router) {
            this._router = new Router({
                caseSensitive: this.enabled('case sensitive routing'),
                strict: this.enabled('strict routing')
            })
        }

        this._router.use(initMiddleware(this));
    }

    public handle(req: Request, res: Response, callback?: Function) {

    }

    public use() {}

    public route() {}

    public engine() {}

    public param() {}

    public set(param: string, value?: any) {}

    public render() {}

    public listen(port?: 8080) {}

    public start() {}

    private applyDefaults() {
        let env = process.env.NODE_ENV || 'development';

        this.enable('x-powered-by');
        this.set('etag', 'weak');
        this.set('env', env);
        this.set('query parser', 'extended');
        this.set('subdomain offset', 2);
        this.set('trust proxy', false);

        this.log.debug(`booting in ${env} mode`);

        this.on('mount', (parent: Application) => {
            // Inherit trust proxy from parent
            if (this.settings[trustProxyDefaultSymbol] === true
                && _.isFunction(parent.settings['trust proxy fn'])) {
                    delete this.settings['trust proxy'];
                    delete this.settings['trust proxy fn'];
            }

            // Inherit request, response, engines, and settings
            this.request = parent.request;
            this.response = parent.response;
            this.engines = parent.engines;
            this.settings = parent.settings;
        });

        // Create locals object
        this.locals = Object.create(null);

        // Topmost app is mounted at '/'
        this.mountpath = '/';

        // Add app's settings to locals
        this.locals['settings'] = this.settings;

        // Enable view caching on production
        if (env === 'production') {
            this.enable('view cache');
        }


    }

}