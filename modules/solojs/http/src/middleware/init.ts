import * as http from 'http';

import { Server } from '../server';
import { Response } from '../response';
import { Request } from '../request';

export function init(app: Server) {
    return function init(req: Request, res: Response, next: () => void) {
        res.setHeader('X-Powered-By', 'SoloJs');

        req.res = res;
        res.req = req;
        req.next = next;

        (<any>req).__proto__ = app.request;
        (<any>res).__proto__ = app.response;

        res.locals = res.locals || Object.create(null);

        next();
    }
}