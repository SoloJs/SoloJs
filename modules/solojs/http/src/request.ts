import * as path from 'path';
import * as http from 'http';
import * as net from 'net';

import * as _ from 'lodash';
const accepts = require('accepts');
const fresh = require('fresh');
const parseRange = require('range-parser');
const parseUrl = require('parseurl');
const typeis = require('type-is');
const proxyAddr = require('poxy-addr');

import { Server } from './server';
import { Response } from './response';


export class Request extends http.IncomingMessage {

    public app: Server;

    public res: Response;

    public next: (err?: any) => void;

    public body: Object;

    public query: Object;

    public params: {[key: string]: any};
    
    public secret?: string;

    constructor(app: Server) {
        super();
        this.app = app;
    }

    public get protocol(): string {
        let proto = this.connection['encrypted'] ? 'https' : 'http';
        let trust = this.app.get('trust proxy fn');

        if (!trust(this.connection.remoteAddress, 0)) {
            return proto;
        }

        // Note: X-Forwarded-Proto is normally only ever a
        //       single value, but this is to be safe.
        proto = this.get('X-Forwarded-Proto') || proto;
        return proto.split(/\s*,\s*/)[0];
    }

    public get secure(): boolean {
        return this.protocol === 'https';
    }

    public get ip(): string {
        let trust = this.app.get('trust proxy fn');
        return proxyAddr(this, trust);
    }

    public get ips(): string[] {
        let trust = this.app.get('trust proxy fn');
        let addrs = proxyAddr.all(this, trust);
        return addrs.slice(1).reverse();
    }

    public get subdomains(): string[] {
        let hostname = this.hostname;

        if (!hostname) return;

        let offset = this.app.get('subdomain offset');
        let subdomains = !net.isIP(hostname) ? hostname.split('.').reverse() : [hostname];

        return subdomains.slice(offset);
    }

    public get path(): string {
        return parseUrl(this).pathname;
    }

    public get hostname(): string {
        let trust = this.app.get('trust proxy fn');
        let host = this.get('X-Forwarded-Host');

        if (!host || !trust(this.connection.remoteAddress, 0)) {
            host = this.get('Host');
        }

        if (!host) return;

        // IPv6 literal support
        let offset = host[0] === '[' ? host.indexOf(']') + 1 : 0;
        let index = host.indexOf(':', offset);

        return index !== -1 ? host.substring(0, index) : host;
    }

    public get fresh(): boolean {
        let method = this.method;
        let s = this.res.statusCode;

        // GET or HEAD for weak freshness validation only
        if ('GET' !== method && 'HEAD' !== method) return false;

        // 2xx or 304 as per rfc2616 14.26
        if ((s >= 200 && s < 300) || 304 === s) {
            // TODO: Figure out if res._headers exists
            // return fresh(this.headers, (this.res._headers || {}));
            return fresh(this.headers, {});
        }

        return false;
    }

    public get stale(): boolean {
        return !this.fresh;
    }

    public get xhr(): boolean {
        let val = this.get('X-Requested-With') || '';
        return val.toLowerCase() === 'xmlhttprequest';
    }

    public get(name: string): any {
        if (!name) {
            throw new TypeError('A name is required for get');
        }

        if (!_.isString(name)) {
            throw new TypeError('Name must be a string');
        }

        let lc = name.toLowerCase();

        switch (lc) {
            case 'referer':
            case 'referrer':
            return this.headers['referrer']
                || this.headers['referer'];
            default:
            return this.headers[lc];
        }
    }
    public header = this.get;

    public accepts(...args: any[]): string | string[] | boolean {
        let accept = accepts(this);
        return accept.types.apply(accept, args);
    }

    public acceptsEncodings(...args: string[]): string | string[] {
        let accept = accepts(this);
        return accept.encodings.apply(accept, arguments);
    }

    public acceptsCharsets(...args: string[]): string | string[] {
        let accept = accepts(this);
        return accept.charset.apply(accept, arguments);
    }

    public acceptsLanguages(...args: string[]): string | string[] {
        let accept = accepts(this);
        return accept.languages.apply(accept, arguments);
    }

    public range(size: number, options: any): number | number[] {
        let range = this.get('Range');
        if (!range) return;
        return parseRange(size, range, options);
    }

    public param<T>(name: string, defaultValue: T): T {
        let params = this.params || {};
        let body = this.body || {};
        let query = this.query || {};

        let args = arguments.length === 1 ? 'name' : 'name, default';

        if (null != params[name] && params.hasOwnProperty(name)) return params[name];
        if (null != body[name]) return body[name];
        if (null != query[name]) return query[name];

        return defaultValue;
    }

    public is(types: string | string[]): string | false | null {
        let arr = types;

        // support flattened arguments
        if (!Array.isArray(types)) {
            arr = new Array(arguments.length);
            for (let i = 0; i < arr.length; i++) {
            arr[i] = arguments[i];
            }
        }

        return typeis(this, arr);
    }


}