import * as _path from 'path';
import * as _ from 'lodash';
import { inspect } from 'util';

import { log } from '../../core';
import { Route, RouteOptions } from './route';

export interface RouterOptions {
    ns?: string;
    caseSensitive?: boolean;
    strict?: boolean;
}

/**
 * Provides an interface for the creation of
 * routes. This get's passe into the
 * route creation function on the
 * `routes.js` page.
 * 
 * @export
 * @class Router
 */
export class Router {

    /**
     * List of compiled routes.
     * 
     * @type {Route[]}
     * @memberOf Router
     */
    public routes: Route[];

    /**
     * Namespace for the handler.
     * 
     * @private
     * @type {string}
     * @memberOf Router
     */
    private ns: string;

    private caseSensitive: boolean;

    private strict: boolean;

    /**
     * Creates an instance of Router.
     * 
     * @param {string} [ns]
     * 
     * @memberOf Router
     */
    constructor(options?: RouterOptions) {
        this.routes = [];
        this.caseSensitive = options.caseSensitive || false;
        this.strict = options.strict || false;
        this.ns = options.ns || '';
    }

    /**
     * Routes get requests for the application
     * root to the specified controller and
     * action.
     * 
     * @param {string} path
     * @param {(string | RouteOptions)} to
     * @param {RouteOptions} [options]
     * 
     * @memberOf Router
     */
    public root(to: string): void {
        this.get('/', to);
    }

    /**
     * Routes GET requests to the specified
     * controller and action.
     * 
     * @param {string} path
     * @param {(string | RouteOptions)} to
     * @param {RouteOptions} [options]
     * 
     * @memberOf Router
     */
    public get(path: string, options: RouteOptions);
    public get(path: string, to: string, options?: RouteOptions);
    public get(path: string, to: string | RouteOptions, options?: RouteOptions): void {
        path = this.parsePath(path);
        options = this.parseParams(to, 'get', options);
        let route = new Route(path, options);
        this.routes.push(route);
    }

    /**
     * Routes POST requests to the specified
     * controller and action.
     * 
     * @param {string} path
     * @param {(string | RouteOptions)} to
     * @param {RouteOptions} [options]
     * 
     * @memberOf Router
     */
    public post(path: string, options: RouteOptions);
    public post(path: string, to: string, options?: RouteOptions);
    public post(path: string, to: string | RouteOptions, options?: RouteOptions): void {
        path = this.parsePath(path);
        options = this.parseParams(to, 'post', options);
        let route = new Route(path, options);
        this.routes.push(route);
    }

    /**
     * Routes PUT requests to the specified
     * controller and action.
     * 
     * @param {string} path
     * @param {(string | RouteOptions)} to
     * @param {RouteOptions} [options]
     * 
     * @memberOf Router
     */
    public put(path: string, options: RouteOptions);
    public put(path: string, to: string, options?: RouteOptions);
    public put(path: string, to: string | RouteOptions, options?: RouteOptions): void {
        path = this.parsePath(path);
        options = this.parseParams(to, ['put', 'patch'], options);
        let route = new Route(path, options);
        this.routes.push(route);
    }

    /**
     * Routes PATCH requests to the specified
     * controller and action.
     * 
     * @param {string} path
     * @param {(string | RouteOptions)} to
     * @param {RouteOptions} [options]
     * 
     * @memberOf Router
     */
    public patch = this.put;

    /**
     * Routes DELETE requests to the specified
     * controller and action.
     * 
     * @param {string} path
     * @param {(string | RouteOptions)} to
     * @param {RouteOptions} [options]
     * 
     * @memberOf Router
     */
    public delete(path: string, options: RouteOptions);
    public delete(path: string, to: string, options?: RouteOptions);
    public delete(path: string, to: string | RouteOptions, options?: RouteOptions): void {
        path = this.parsePath(path);
        options = this.parseParams(to, 'delete', options);
        let route = new Route(path, options);
        this.routes.push(route);
    }

    /**
     * Routes any type of request (via) to the
     * specified controller and action
     * 
     * @param {string} path
     * @param {(string | string[] | RouteOptions)} to
     * @param {(string | string[] | RouteOptions)} [via]
     * @param {RouteOptions} [options]
     * 
     * @memberOf Router
     */
    public match(path: string, options: RouteOptions);
    public match(path: string, to: string, options?: RouteOptions);
    public match(path: string, to: string | string[] | RouteOptions, via?: string | string[] | RouteOptions, options?: RouteOptions): void {
        path = this.parsePath(path);
        options = this.parseParams(to, via, options);
        let route = new Route(path, options);
        this.routes.push(route);
    }

    /**
     * 
     * 
     * @param {(string | RouteOptions)} controller
     * @param {RouteOptions} [options]
     * 
     * @memberOf Router
     */
    public resources(controller: string, options?: RouteOptions): void {
        let resourceRoutes = [
            { via: 'get',               route: '/' + controller,                        to: `${controller}#index` },
            { via: 'get',               route: '/' + controller + '/new',     to: `${controller}#new` },
            { via: 'post',              route: '/' + controller,                        to: `${controller}#create` },
            { via: 'get',               route: '/' + controller + '/:id',     to: `${controller}#show` },
            { via: 'get',               route: '/' + controller + '/:id/edit',to: `${controller}#edit` },
            { via: ['patch', 'put'],    route: '/' + controller + '/:id',     to: `${controller}#update` },
            { via: 'delete',            route: '/' + controller + '/:id',     to: `${controller}#destroy` }
        ];

        for (let _route of resourceRoutes) {
            let opts: RouteOptions = {
                to: _route.to,
                via: _route.via
            }; 
            let route = new Route(_route.route, opts);
            this.routes.push(route);
        }
    }

    /**
     * Creates a sub-handler with the specified namespace.
     * All routes created within will have the
     * namespace applied.
     * 
     * @example
     * route.namespace('/api/v1', (route: Router) => {
     *     route.get('applications', 'applications#index');
     *     // Route { ...path: 'api/v1/applications' }
     * });
     * 
     * @param {string} namespace
     * @param {(hander: Router) => any} callback
     * 
     * @memberOf Router
     */
    public namespace(namespace: string, callback: (hander: Router) => any): void {
        let handler = new Router(namespace);
        callback.call(this, handler);
        this.routes = this.routes.concat(handler.routes);
    }

    public parse(path: string, method: string): Route | false {
        method = method.toLowerCase();
        let routes: Route[] = _.reverse(this.routes);
        for (let route of routes) {
            if (route.match(path) && _.includes(route.method, method)) {
                return route;
            }
        }
        return false;
    }

    /**
     * Generates a path based on the handler's
     * namespace.
     * 
     * @private
     * @param {string} pth
     * @returns
     * 
     * @memberOf Router
     */
    private parsePath(pth: string) {
        if (this.ns)
            pth = _path.join(this.ns, pth);
        return pth;
    }

    /**
     * Utility method for each of the routing
     * functions. Provides abstraction.
     * 
     * @private
     * @param {(string | string[] | RouteOptions)} to
     * @param {(string | string[] | RouteOptions)} [via]
     * @param {RouteOptions} [options]
     * @returns {RouteOptions}
     * 
     * @memberOf Router
     */
    private parseParams(to: string | string[] | RouteOptions, via?: string | string[] | RouteOptions, options?: RouteOptions): RouteOptions {
        if (_.isPlainObject(to)) {
            return <RouteOptions>to;
        }

        if (_.isPlainObject(via) && _.isString(to)) {
            options = <RouteOptions>via;
            via = null;
            options.to = to;
            return options;
        }

        if (_.isObject(options) && _.isString(to) && (_.isString(via) || _.isArray(via))) {
            options.via = via;
            options.to = to;
            return options;
        }

        if (_.isString(to) && _.isString(via)) {
            return {
                to: to,
                via: via
            };
        }

        throw new Error(`parseParams failed: Wrong parameters supplied.\nThe supplied parameters were (${to}, ${via}, ${options})`);
    }

}
