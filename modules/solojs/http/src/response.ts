import * as http from 'http';
import * as path from 'path';

import * as _ from 'lodash';
const onFinished = require('on-finished');
const send = require('send');
const cookie = require('cookie');
const vary = require('vary');
const escapeHtml = require('escape-html');
const encodeUrl = require('encodeurl');
const sign = require('cookie-signature').sign;
const mime = send.mime;
const contentDisposition = require('content-disposition');

import { setCharset, stringify, isAbsolute, normalizeType, normalizeTypes } from './utils';
import { Request } from './request';
import { Server } from './server';

let charsetRegExp = /;\s*charset\s*=/;

export type Sendable = string | number | boolean | Object | Buffer;

export class Response extends http.ServerResponse {

    public app: Server;

    public req: Request;

    public charset: string;

    public locals: Object;

    constructor(app: Server) {
        super();
        this.app = app;
    }

    public status(code: number): this {
        this.statusCode = code;
        return this;
    }

    public links(links): this {
        let link = this.get('Link') || '';
        if (link) link += ', ';
        return this.set('Link', link + Object.keys(links).map(function(rel){
            return '<' + links[rel] + '>; rel="' + rel + '"';
        }).join(', '));
    }

    public send(body: Sendable)
    public send(status: number, body: Sendable);
    public send(status: number | Sendable, body?: Sendable): this {
        let chunk = !_.isNumber(status) ? status : body;
        status = _.isNumber(status) ? status : undefined;
        let encoding;
        let len;
        let req = this.req;
        let type;

        // settings
        let app = this.app;

        if (_.isNumber(status)) {
            this.statusCode = status;
        }

        switch (typeof chunk) {
            // string defaulting to html
            case 'string':
            if (!this.get('Content-Type')) {
                this.type('html');
            }
            break;
            case 'boolean':
            case 'number':
            case 'object':
            if (chunk === null) {
                chunk = '';
            } else if (Buffer.isBuffer(chunk)) {
                if (!this.get('Content-Type')) {
                this.type('bin');
                }
            } else {
                return this.json(chunk);
            }
            break;
        }

        // write strings in utf-8
        if (typeof chunk === 'string') {
            encoding = 'utf8';
            type = this.get('Content-Type');

            // reflect this in content-type
            if (typeof type === 'string') {
            this.set('Content-Type', setCharset(type, 'utf-8'));
            }
        }

        // populate Content-Length
        if (chunk !== undefined) {            
            if (!Buffer.isBuffer(chunk)) {
            // convert chunk to Buffer; saves later double conversions
            chunk = new Buffer(<string>chunk, encoding);
            encoding = undefined;
            }

            len = (<Buffer>chunk).length;
            this.set('Content-Length', len);
        }

        // populate ETag
        let etag;
        let generateETag = len !== undefined && app.get('etag fn');
        if (typeof generateETag === 'function' && !this.get('ETag')) {
            if ((etag = generateETag(chunk, encoding))) {
            this.set('ETag', etag);
            }
        }

        // freshness
        if (req.fresh) this.statusCode = 304;

        // strip irrelevant headers
        if (204 === this.statusCode || 304 === this.statusCode) {
            this.removeHeader('Content-Type');
            this.removeHeader('Content-Length');
            this.removeHeader('Transfer-Encoding');
            chunk = '';
        }

        if (req.method === 'HEAD') {
            // skip body for HEAD
            this.end();
        } else {
            // respond
            this.end(chunk, encoding);
        }

        return this;
    }

    public json(obj: string | number | boolean | Object): this {
        let val = obj;

        // allow status / body
        if (arguments.length === 2) {
            // res.json(body, status) backwards compat
            if (typeof arguments[1] === 'number') {
            this.statusCode = arguments[1];
            } else {
            this.statusCode = arguments[0];
            val = arguments[1];
            }
        }

        // settings
        let app = this.app;
        let replacer = app.get('json replacer');
        let spaces = app.get('json spaces');
        let body = stringify(val, replacer, spaces);

        // content-type
        if (!this.get('Content-Type')) {
            this.set('Content-Type', 'application/json');
        }

        return this.send(body);
    }

    public jsonp(obj: string | number | boolean | Object): this {
        let val = obj;

        // allow status / body
        if (arguments.length === 2) {
            // res.json(body, status) backwards compat
            if (typeof arguments[1] === 'number') {
            this.statusCode = arguments[1];
            } else {
            this.statusCode = arguments[0];
            val = arguments[1];
            }
        }

        // settings
        let app = this.app;
        let replacer = app.get('json replacer');
        let spaces = app.get('json spaces');
        let body = stringify(val, replacer, spaces);
        let callback = this.req.query[app.get('jsonp callback name')];

        // content-type
        if (!this.get('Content-Type')) {
            this.set('X-Content-Type-Options', 'nosniff');
            this.set('Content-Type', 'application/json');
        }

        // fixup callback
        if (Array.isArray(callback)) {
            callback = callback[0];
        }

        // jsonp
        if (typeof callback === 'string' && callback.length !== 0) {
            this.charset = 'utf-8';
            this.set('X-Content-Type-Options', 'nosniff');
            this.set('Content-Type', 'text/javascript');

            // restrict callback charset
            callback = callback.replace(/[^\[\]\w$.]/g, '');

            // replace chars not allowed in JavaScript that are in JSON
            body = body
            .replace(/\u2028/g, '\\u2028')
            .replace(/\u2029/g, '\\u2029');

            // the /**/ is a specific security mitigation for "Rosetta Flash JSONP abuse"
            // the typeof check is just to reduce client error noise
            body = '/**/ typeof ' + callback + ' === \'function\' && ' + callback + '(' + body + ');';
        }

        return this.send(body);
    }

    public sendStatus(statusCode) {
        let body = http.STATUS_CODES[statusCode] || String(statusCode);

        this.statusCode = statusCode;
        this.type('txt');

        return this.send(body);
    }

    public sendFile(path, options, callback) {
        let done = callback;
        let req = this.req;
        let res = this;
        let next = req.next;
        let opts = options || {};

        if (!path) {
            throw new TypeError('path argument is required to res.sendFile');
        }

        // support function as second arg
        if (typeof options === 'function') {
            done = options;
            opts = {};
        }

        if (!opts.root && !isAbsolute(path)) {
            throw new TypeError('path must be absolute or specify root to res.sendFile');
        }

        // create file stream
        let pathname = encodeURI(path);
        let file = send(req, pathname, opts);

        // transfer
        sendfile(res, file, opts, function (err) {
            if (done) return done(err);
            if (err && err.code === 'EISDIR') return next();

            // next() all but write errors
            if (err && err.code !== 'ECONNABORTED' && err.syscall !== 'write') {
                next(err);
            }
        });
    }

    public download(path, filename, callback) {
        let done = callback;
        let name = filename;

        // support function as second arg
        if (typeof filename === 'function') {
            done = filename;
            name = null;
        }

        // set Content-Disposition when file is sent
        let headers = {
            'Content-Disposition': contentDisposition(name || path)
        };

        // Resolve the full path for sendFile
        let fullPath = path.resolve(path);

        return this.sendFile(fullPath, { headers: headers }, done);
    }

    public contentType(type) {
        let ct = type.indexOf('/') === -1
            ? mime.lookup(type)
            : type;

        return this.set('Content-Type', ct);
    }
    public type = this.contentType;

    format = function(obj){
        let req = this.req;
        let next = req.next;

        let fn = obj.default;
        if (fn) delete obj.default;
        let keys = Object.keys(obj);

        let key = keys.length > 0
            ? req.accepts(keys)
            : false;

        this.lety("Accept");

        if (key) {
            this.set('Content-Type', normalizeType(key).value);
            obj[key](req, this, next);
        } else if (fn) {
            fn();
        } else {
            let err = new Error('Not Acceptable');
            err['status'] = err['statusCode'] = 406;
            err['types'] = normalizeTypes(keys).map(function(o){ return o.value });
            next(err);
        }

        return this;
    }

    public attachment(filename): this {
        if (filename) {
            this.type(path.extname(filename));
        }

        this.set('Content-Disposition', contentDisposition(filename));

        return this;
    }

    public append(field, val): this {
        let prev = this.get(field);
        let value = val;

        if (prev) {
            // concat the new and prev vals
            value = Array.isArray(prev) ? prev.concat(val)
            : Array.isArray(val) ? [prev].concat(val)
            : [prev, val];
        }

        return this.set(field, value);
    }

    public header(options: Object);
    public header(field: string, val: number);
    public header(field: string, val: string);
    public header(field: string, val: string[]);
    public header(field: string | Object, val?: string | string[] | number): this {
        if (_.isNumber(val)) {
            val = String(val);
        }

        if (_.isPlainObject(field)) {
            for (let key in <Object>field) {
                this.set(key, field[key]);
            }
        } else if (_.isString(val) && _.isString(field)) {
            // add charset to content-type
            if (field.toLowerCase() === 'content-type' && !charsetRegExp.test(val)) {
                let charset = mime.charsets.lookup(val.split(';')[0]);
                if (charset) val += '; charset=' + charset.toLowerCase();
            }

            this.setHeader(field, val);
        } else if (_.isArray(val) && _.isString(field)) {
            this.setHeader(field, val);
        } else {
            throw new TypeError(`Can not set header to (${field}, ${val})`);
        }

        return this;
    }
    public set = this.header;

    public get(field: string): string {
        return this.getHeader(field);
    }

    public clearCookie(name, options): this {
        let opts = _.merge({ expires: new Date(1), path: '/' }, options);

        return this.cookie(name, '', opts);
    }

    public cookie(name: string, value: any, options?: Object): this {
        let opts = _.merge({}, options);
        let secret = this.req.secret;
        let signed = opts['signed'];

        if (signed && !secret) {
            throw new Error('cookieParser("secret") is required for signed cookies');
        }

        let val = _.isObject(value) ? 'j:' + JSON.stringify(value): String(value);

        if (signed) {
            val = 's:' + sign(val, secret);
        }

        if ('maxAge' in opts) {
            opts['expires'] = new Date(Date.now() + opts['maxAge']);
            opts['maxAge'] /= 1000;
        }

        if (!opts['path']) {
            opts['path'] = '/'
        }

        this.append('Set-Cookie', cookie.serialize(name, String(val), opts));

        return this;
    }

    
    public location(url: string): this {
        let loc = url;

        if (url === 'back') {
            loc = this.req.get('Referrer') || '/';
        }

        return this.set('Location', encodeUrl(loc));
    }

    public redirect(url: string);
    public redirect(status: number, url: string);
    public redirect(status: string | number, url?: string): void {
        let body = '';
        url = _.isString(url) ? url : <string>status;
        status = _.isNumber(status) ? status : 302;

        if (!url || !_.isString(url)) {
            throw new TypeError('Url argument is required for a redirect'); 
        }

        let address = this.location(url).get('Location');

        this.format({
            text: () => {
                body = `${http.STATUS_CODES[status]}. Redirecting to ${address}`;
            },
            html: () => {
                let u = escapeHtml(address);
                body = `<p>${http.STATUS_CODES[status]}. Redirecting to <a href="${u}">${u}</a></p>`;
            },
            default: () => {
                body = '';
            }
        });

        this.statusCode = status;
        this.set('Content-Length', Buffer.byteLength(body));

        if (this.req.method === 'HEAD') {
            this.end();
        } else {
            this.end(body);
        }
    }

    public vary(field: string | string[]): this {
        if (!field || (_.isArray(field) && !field.length)) {
            return this;
        }

        vary(this, field);

        return this;
    }

    public render(view: string, options?: Object | Function, callback?: Function): void {
        let app = this.req.app;
        let done = callback;
        let opts = options || {};
        let req = this.req;
        let self = this;

        if (_.isFunction(options)) {
            done = options;
            opts = {};
        }

        opts['_locals'] = self.locals;

        done = done || function(err: Error, str: string) {
            if (err) return req.next(err);
            self.send(str);
        };

        app.render(view, opts, done);
    }

}

export interface CookieOptions {
    signed?: boolean;
    maxAge?: number;
    expires?: Date;
    path?: string;
}

function sendfile(res, file, options, callback) {
  let done = false;
  let streaming;

  // request aborted
  function onaborted() {
    if (done) return;
    done = true;

    let err = new Error('Request aborted');
    err['code'] = 'ECONNABORTED';
    callback(err);
  }

  // directory
  function ondirectory() {
    if (done) return;
    done = true;

    let err = new Error('EISDIR, read');
    err['code'] = 'EISDIR';
    callback(err);
  }

  // errors
  function onerror(err) {
    if (done) return;
    done = true;
    callback(err);
  }

  // ended
  function onend() {
    if (done) return;
    done = true;
    callback();
  }

  // file
  function onfile() {
    streaming = false;
  }

  // finished
  function onfinish(err) {
    if (err && err.code === 'ECONNRESET') return onaborted();
    if (err) return onerror(err);
    if (done) return;

    setImmediate(function () {
      if (streaming !== false && !done) {
        onaborted();
        return;
      }

      if (done) return;
      done = true;
      callback();
    });
  }

  // streaming
  function onstream() {
    streaming = true;
  }

  file.on('directory', ondirectory);
  file.on('end', onend);
  file.on('error', onerror);
  file.on('file', onfile);
  file.on('stream', onstream);
  onFinished(res, onfinish);

  if (options.headers) {
    // set headers on successful transfer
    file.on('headers', function headers(res) {
      let obj = options.headers;
      let keys = Object.keys(obj);

      for (let i = 0; i < keys.length; i++) {
        let k = keys[i];
        res.setHeader(k, obj[k]);
      }
    });
  }

  // pipe
  file.pipe(res);
}