import * as http from 'http';
import * as https from 'https';
import * as path from 'path';
import * as fs from 'fs';
import * as querystring from 'querystring';

import * as _ from 'lodash';

import { Application } from './application';
import { Request } from './request';
import { Response } from './response';
import { RouteHandler } from '../../control';
import { Loggable } from '../../core';

export type Middleware = (req: http.IncomingMessage, res: http.ServerResponse, next: () => void) => void;

export class Server extends Application {

    public request: Request;

    public response: Response;

    constructor(options?: any) {
        super();

        this.emit('initialize');
    }


}