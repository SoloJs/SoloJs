import * as fs from 'fs';
import * as path from 'path';

import * as cson from 'cson';
import * as yaml from 'js-yaml';

export class i18n {

    public locale: string;

    public locales: Object;

    protected searchPaths: string[];

    constructor(locale: string, searchPaths: string | string[]) {
        
        this.locale = locale;

        if (typeof searchPaths === 'string') {
            this.searchPaths = [searchPaths];
        } else {
            this.searchPaths = searchPaths;
        }

        this.locales = this.parseLocales();

    }

    public setLocale(locale: string) {
        this.locale = locale;
    }

    public get currentLocale() {
        return this.locales[this.locale];
    }

    public get(key: string): string | null {
        return this.currentLocale ? this.currentLocale[key] : null;
    }

    public set(key: string, value?: string) {
        this.locales[this.locale][key] = value;
    }

    /**
     * Parse locales in all search paths. locales
     * can be of json, js, yaml, or cson format 
     * 
     * @memberOf i18n
     */
    protected parseLocales(): Object {
        let files: string[] = [];
        let locales = {};
        
        for (let p of this.searchPaths) {
            let f = fs.readdirSync(p).map((f) => path.join(p, f));
            files = files.concat(f);
        }

        for (let file of files) {
            let ext = path.extname(file);
            let contents = {};

            if (ext === '.yaml' || ext === '.yml') {
                let str = fs.readFileSync(file).toString('utf8');
                contents = yaml.safeLoad(str);
            }
            else if (ext === '.cson')
                contents = cson.parseCSONFile(file);
            else if (ext === '.js' || ext === '.json')
                contents = require(file);
            else
                continue;

            for (let attrname in contents) {
                locales[attrname] = contents[attrname];
            }
        }

        return locales;
    }

}