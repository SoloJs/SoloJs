export function deprecated(target: Function, key: string, value: any) {
    console.log(`Deprecation warning: The method "${key}" is deprecated.`);
    return target;
}