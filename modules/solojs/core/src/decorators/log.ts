import { Loggable } from '../logger'

/**
 * Log decorator. Logs the result of a method call.
 * Example:
 *  ```
 *  class MyClass {
 *  
 *      @log()
 *      public static someMethod(arg: string) {
 *             return arg;
 *      }
 * 
 *  }
 * 
 *  MyClass.someMethod('Hello world');
 *  // Call: someMethod('Hello world') => "Hello world"
 *  ```
 * 
 * @export
 * @param {Function} target
 * @param {string} key
 * @param {*} value
 * @returns
 */
export function log(logger?: Loggable) {
    if (!logger) logger = console;
    return (target: Object, key: string, value: any) => {
        return {
            value: function(...args: any[]) {
                let a = args.map(a => JSON.stringify(a)).join(', ');
                let result = value.value.apply(this, args);
                let res = JSON.stringify(result);
                logger.log(`Call: ${key}(${a}) => ${res}`);
                return result;
            }
        }
    }
}