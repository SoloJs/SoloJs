import { Logger as Winston, LoggerOptions, TransportInstance, transports } from 'winston';

export { transports };

export const LOG_LEVEL = {
    SILLY: 'silly',
    DEBUG: 'debug',
    VERBOSE: 'verbose',
    INFO: 'info',
    WARN: 'warn',
    ERROR: 'error'
}

export interface Loggable {
    log(...args: any[]): void;
}

export class Logger extends Winston {

    constructor(logLevel: string, options?: LoggerOptions) {
        super(options || Logger.defaults(logLevel));
    }

    public static defaults(logLevel: string): LoggerOptions {
        return {
            transports: [
                new transports.Console({
                    level: logLevel
                })
            ],
            exceptionHandlers: [
                new transports.File({
                    dirname: process.cwd(),
                    filename: 'solocrash.log'
                })
            ],
            exitOnError: (err) => {
                console.error('A fatal error has occured. See "solocrash.log" for more info.');
                return true;
            }
        }
    }
}