import { EventEmitter } from 'events';

import * as nunjucks from 'nunjucks';

export interface ViewEngineOptions {
    
    /**
     * Absolute path to template directory.
     * 
     * @type {string}
     * @memberOf ViewEngineOptions
     */
    templatePaths: string | string[];

    /**
     * Makes console output verbose.
     * 
     * @type {boolean}
     * @memberOf ViewEngineOptions
     */
    debug?: boolean;
}

export class ViewEngine extends EventEmitter {

    /**
     * Instance of the nunjucks environment.
     * 
     * @protected
     * @type {nunjucks.Environment}
     * @memberOf ViewEngine
     */
    protected engine: nunjucks.Environment;

    /**
     * Absolute path to template directory.
     * 
     * @protected
     * @type {string}
     * @memberOf ViewEngine
     */
    protected templatePaths: string | string[];

    /**
     * Makes console output verbose.
     * 
     * @protected
     * @type {boolean}
     * @memberOf ViewEngine
     */
    protected debug: boolean;

    /**
     * Creates an instance of ViewEngine.
     * 
     * @param {ViewEngineOptions} options
     * 
     * @memberOf ViewEngine
     */
    constructor(options: ViewEngineOptions) {
        super();

        this.templatePaths = options.templatePaths;
        this.debug = options.debug || false;

        let loader = new nunjucks.FileSystemLoader(this.templatePaths);
        this.engine = new nunjucks.Environment(loader);

        this.emit('loaded');
    }

    public render(template: string, context: Object, callback?: (err: any, res: string) => any) {
        this.emit('render', { type: 'file', template: template, context: context });
        return this.engine.render(template, context, callback);
    }

    public renderString(template: string, context: Object, callback?: (err: any, res: string) => any) {
        this.emit('render', { type: 'string', template: template, context: context });
        return this.engine.renderString(template, context, callback);
    }

    public registerExtension(name: string, extension: nunjucks.Extension) {
        this.emit('addExtension', name);
        this.engine.addExtension(name, extension);
    }

    public registerFilter(name: string, func: (...args: any[]) => any, async?: boolean) {
        this.emit('addFilter', name);
        this.engine.addFilter(name, func, async);
    }

    public registerGlobal(name: string, value?: any) {
        this.emit('addGlobal', { name: value });
        this.engine.addGlobal(name, value);
    }

}