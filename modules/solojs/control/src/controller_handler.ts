import * as util from 'util';
import * as path from 'path';
const walk = require('walk');
const Case = require('case');

import { Controller } from './controller';

export interface ControllerHandlerOptions {
    controllerRoot: string;
}

export class ControllerHandler {

    protected controllerRoot: string;

    protected controllers: {[key: string]: Controller}[];

    constructor(options?: ControllerHandlerOptions) {

        this.controllerRoot = options.controllerRoot;
        this.getControllers();
    }

    public call(controller: string, method: string, ...args: any[]): any {
        if (!this.controllers[controller])
            throw new Error(`Controller '${controller}' doesn't exist`);

        let ctrl = this.controllers[controller];

        if (!(method in ctrl))
            throw new Error(`Method '${method}' does not exist on controller '${controller}'`);

        return ctrl[method].call(this, ...args);
    }

    public apply(controller: string, method: string, ...args: any[]): any {
        if (!this.controllers[controller])
            throw new Error(`Controller '${controller}' doesn't exist`);

        let ctrl = this.controllers[controller];

        if (!(method in ctrl))
            throw new Error(`Method '${method}' does not exist on controller '${controller}'`);

        return ctrl[method].apply(this, args);
    }

    private getControllers() {
        let controllers: {[key: string]: Controller}[] = this.controllers = [];
        let walkerOptions = {
            listeners: {
                file: (root, stats, next) => {
                    if (stats.name.indexOf('.controller') < 0) {
                        return next();
                    }

                    // hello_world
                    let snakeCaseControllerName: string = stats.name.substring(0, stats.name.indexOf('.'));
                    // HelloWorld
                    let pascalCaseControllerName: string = Case.pascal(snakeCaseControllerName);
                    // HelloWorldController
                    let controllerName: string = pascalCaseControllerName + 'Controller';

                    // Absolute path to the controller
                    let controllerPath: string = path.join(root, stats.name);

                    // Namespace to use as a key
                    let namespace = path.relative(this.controllerRoot, path.join(root, snakeCaseControllerName));

                    let controller: Controller;
                    try {
                        let Ctrl = require(controllerPath);
                        controller = new Ctrl();
                    } catch (e) {
                        // TODO: Replace this with an actual log
                        console.error(e);
                        return next();
                    }

                    controllers[namespace] = controller;
                    next();
                },
                errors: function (root, nodeStatsArray, next) {
                    next();
                }
            }
        }

        walk.walkSync(this.controllerRoot, walkerOptions);
    }

}

// let c = new ControllerHandler({ controllerRoot: '/home/chris/Documents/Projects/node-solo/test-app/app/controllers' });
// c.apply('welcome', 'index', 'Hello', 'world');